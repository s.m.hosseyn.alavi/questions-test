import React, { useState, useEffect } from "react";
import { styles } from "../AskToStart/styles";
import { Paper, Typography, Button } from "@material-ui/core";
import { useSelector } from "react-redux";

export default function Score({ handleStart }) {
  const questions = useSelector(state => state.glossary.questions);
  const [score, setScore] = useState({});

  useEffect(() => {
    let score = { correct: 0, wrong: 0, noAnswer: 0 };
    questions.map(item => {
      //check if user answered this questiion or not
      if (item.userAnswer) {
        //find the option wich user selected
        const option = item.answers.find(el => el.answerId === item.userAnswer);
        //check if we have the option  or not , by the code logic we have to
        //have an option here but for sure
        if (option) {
          //check if it is a currect choice or not
          if (option.isCorrect) {
            score = { ...score, correct: score.correct + 1 };
          } else {
            score = { ...score, wrong: score.wrong + 1 };
          }
        }
      } else {
        score = { ...score, noAnswer: score.noAnswer + 1 };
      }
      return item;
    });
    setScore(score);
  }, [questions]);

  const classes = styles();

  return (
    <Paper className={classes.paper}>
      <Typography variant="h5"></Typography>
      <Typography component="div">
        Number of correct answers : {score.correct}
        <br />
        Number of wrong answers : {score.wrong}
        <br />
        Number of empty answers : {score.noAnswer}
      </Typography>
      <Button variant="contained" color="primary" onClick={() => handleStart()}>
        Start Over
      </Button>
    </Paper>
  );
}
