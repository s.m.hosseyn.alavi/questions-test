import React from "react";
import { CircularProgress } from "@material-ui/core";

//simple spin component for showing loading
const Spinnig = React.forwardRef(({ children, ...props }, ref) => {
  return (
    <div
      style={{
        position: "relative",
        minHeight: "85%",
        pointerEvents: props.loading === "true" ? "none" : "auto"
      }}
      {...props}
    >
      {props.loading === "true" && (
        <div
          style={{
            top: 0,
            left: 0,
            zIndex: 4,
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            position: "absolute",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(255, 255, 255, 0.8)"
          }}
        >
          <CircularProgress />
        </div>
      )}

      {children}
    </div>
  );
});

export default Spinnig;
