import React from "react";
import { styles } from "./styles";
import { Paper, Typography, Button } from "@material-ui/core";

export default function AskToStart({ handleStart }) {
  const classes = styles();
  return (
    <Paper className={classes.paper}>
      <Typography>Are you ready to start the quiz?</Typography>
      <Button variant="contained" color="primary" onClick={() => handleStart()}>
        Start Quiz
      </Button>
    </Paper>
  );
}
