import { makeStyles } from "@material-ui/core/styles";

export const styles = makeStyles(theme => {
  return {
    paper: {
      [theme.breakpoints.down("xs")]: {
        width: "auto"
      },
      width: 500,
      height: 400,
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-around",
      alignItems: "center",
      padding: 20
    }
  };
});
