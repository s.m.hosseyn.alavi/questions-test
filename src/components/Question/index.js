import React, { useState, useEffect } from "react";
import { styles } from "./styles";
import { Paper, Typography, Button, Radio } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import * as Types from "../../store/types";

export default function Question() {
  const [answer, setAnswer] = useState("");
  const [activeQuestion, setActiveQuestion] = useState("");
  const [questionData, setQuestionData] = useState({});
  const classes = styles();
  const activeQuestionInStore = useSelector(
    state => state.glossary.activeQuestion
  );
  const questions = useSelector(state => state.glossary.questions);
  const dispatch = useDispatch();

  useEffect(() => {
    //This side effect runs when qustion is changed
    if (activeQuestionInStore !== activeQuestion) {
      setActiveQuestion(activeQuestionInStore);
      setQuestionData(
        questions.find(item => item.questionNo === activeQuestionInStore)
      );
      //find if user answered this question before or not to show it
      setAnswer(
        questions.find(item => item.questionNo === activeQuestionInStore)
          .userAnswer
      );
    }
  }, [activeQuestionInStore, activeQuestion, questions, questionData]);

  async function handleChange(event) {
    //after each change we will set data in redux store
    //it could be after submition
    //first we remove the current question from all
    const question = questions.filter(item => item.id !== questionData.id);
    //then we add the user answer to question object and push it to questions Array
    question.push({
      ...questionData,
      userAnswer: parseInt(event.target.value)
    });
    //then save it in redux store
    dispatch({ type: Types.QUESTIONS, payload: question });
    await setAnswer(parseInt(event.target.value));
  }

  function handleNext() {
    dispatch({ type: Types.NEXT_QUESTION });
  }

  function handleBack() {
    dispatch({ type: Types.PREV_QUESTION });
  }

  return (
    <Paper className={classes.paper}>
      <Typography variant="h5">
        Question number {questionData.questionNo}
      </Typography>
      <Typography>Choose the correct answer</Typography>
      <Typography>{questionData && questionData.title}</Typography>
      {questionData.answers &&
        questionData.answers.map((item, key) => (
          <div
            key={key}
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Radio
              checked={answer === item.answerId ? true : false}
              onChange={handleChange}
              value={item.answerId}
              name="glossary-answers"
            />
            <Typography component="span">{item.option}</Typography>
          </div>
        ))}
      <div className={classes.btnWrapper}>
        <Button variant="contained" color="primary" onClick={handleBack}>
          Previews Question
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => handleNext(answer)}
        >
          Next Question
        </Button>
      </div>
    </Paper>
  );
}
