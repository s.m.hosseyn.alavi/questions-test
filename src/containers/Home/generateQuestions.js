export function generateQuestion(glossaries) {
  //check if we have anything from server or not
  if (Array.isArray(glossaries) && glossaries.length > 0) {
    //create an empty list to keep our random question ids
    const randomGlossaryNumbers = [];

    //create a loop to fill previews empty list 10 times
    //it could be a function argumant or hard code like this
    while (randomGlossaryNumbers.length < 10) {
      //create random number
      const number = Math.round(Math.random() * glossaries.length);
      //check if that random number has'nt been selected before then keep it
      if (randomGlossaryNumbers.find(item => item === number) === undefined) {
        randomGlossaryNumbers.push(number);
      }
    }

    //find the glossary objects by our random numbers
    //so we have 10 random glossary object
    const randomGlossaries = randomGlossaryNumbers.map(
      item => glossaries[item]
    );

    //create question with our glossaries
    const questions = randomGlossaries.map((item, key) => {
      //define question object
      const q = {
        title: item.excerpt,
        id: item.id,
        questionNo: key + 1,
        userAnswer: null
      };

      //create a random number for currect answer palce
      const randomCorrectOption = Math.ceil(Math.random() * 4);
      const answers = [];
      //create question options
      while (answers.length < 4) {
        //check if we are in currect answer palce or not to keep it
        if (answers.length + 1 === randomCorrectOption) {
          answers.push({
            option: item.title,
            answerId: item.id,
            isCorrect: true
          });
        } else {
          //create a random place for wrong options
          const wrongAnswer =
            glossaries[Math.round(Math.random() * glossaries.length)];
          //check if we hade that wrong option before or not
          const existAnswer = answers.find(el => el.id === wrongAnswer.id);
          if (wrongAnswer.id !== item.id && existAnswer === undefined) {
            answers.push({
              option: wrongAnswer.title,
              answerId: wrongAnswer.id,
              isCorrect: false
            });
          }
        }
      }
      q.answers = answers;

      return q;
    });

    return questions;
  } else {
    console.log("No data found");
  }
}
