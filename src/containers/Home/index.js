import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getGlossaries,
  setQuestions,
  startQuiz
} from "../../store/actions/glossary";
import Spinnig from "../../components/Spin";
import AskToStart from "../../components/AskToStart";
import { styles } from "./styles";
import { generateQuestion } from "./generateQuestions";
import { withStyles } from "@material-ui/styles";
import Question from "../../components/Question";
import Score from "../../components/Score";

class Home extends Component {
  state = {
    loading: false
  };

  //quiz start function
  handleStart = async () => {
    //get all data from server
    await this.getData();
    //send them to create question with this data
    const questions = await generateQuestion(this.props.glossaryList);
    //keep questions in store
    this.props.setQuestions(questions);
    //show quiz component
    this.props.startQuiz();
  };

  getData = async () => {
    this.setState({ loading: true });
    await this.props.getGlossaries();
    this.setState({ loading: false, step: 0 });
  };

  render() {
    return (
      <Spinnig loading={this.state.loading.toString()}>
        <div className={this.props.classes.main}>
          {this.props.activeQuestion === 0 ? (
            <AskToStart handleStart={() => this.handleStart()} />
          ) : this.props.activeQuestion >= 1 &&
            this.props.activeQuestion < 11 ? (
            <Question />
          ) : (
            <Score handleStart={() => this.handleStart()} />
          )}
        </div>
      </Spinnig>
    );
  }
}

const mapStateToProps = ({ glossary }) => ({
  glossaryList: glossary.glossaryList,
  activeQuestion: glossary.activeQuestion
});

export default connect(mapStateToProps, {
  getGlossaries,
  setQuestions,
  startQuiz
})(withStyles(styles)(Home));
