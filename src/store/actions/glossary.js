import * as Types from "../types";
import axios from "axios";

//get data from server and keep them to the store
export const getGlossaries = () => async dispatch => {
  try {
    const response = await axios.get(
      "https://api.binance.vision/api/glossaries/"
    );
    dispatch({ type: Types.GLOSSARY_LIST, payload: response.data });
  } catch {
    console.error("Fail to get data");
  }
};

//get questions from user and keep them to the store
export const setQuestions = data => async dispatch => {
  dispatch({ type: Types.QUESTIONS, payload: data });
};

//begin the quiz
export const startQuiz = () => async dispatch => {
  dispatch({ type: Types.START_QUIZ , payload:0 });
};
