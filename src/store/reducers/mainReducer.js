import { combineReducers } from "redux";
import glossaryReducer from "./glossary";

const mainReducer = combineReducers({
  glossary: glossaryReducer
});

export default mainReducer;
