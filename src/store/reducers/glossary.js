import * as Types from "../types";
const initialState = { glossaryList: [], questions: [], activeQuestion: 0 };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GLOSSARY_LIST:
      return { ...state, glossaryList: [...action.payload] };
    case Types.QUESTIONS:
      return { ...state, questions: [...action.payload] };
    case Types.START_QUIZ:
      return { ...state, activeQuestion: 1 };
    case Types.NEXT_QUESTION:
      return { ...state, activeQuestion: state.activeQuestion + 1 };
    case Types.PREV_QUESTION:
      return { ...state, activeQuestion: state.activeQuestion - 1 };
    default:
      return state;
  }
};

export default reducer;
